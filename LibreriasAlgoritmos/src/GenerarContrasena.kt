class GenerarContrasena {
    var intDimension: Byte = 0
        set(value) {
            field = value
        }
    var blnNumeros: Boolean = false
        set(value) {
            field = value
        }
    var blnSimbolo: Boolean = false
        set(value) {
            field = value
        }

    fun generar(base: String, length: Byte): String {
        var password = ""
        for (i in 0 until length) {
            val random = (0 until base.length).random()
            password += base[random]
        }
        return password
    }

    fun contrasenaAleatoria(): String {
        var base = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
        val numbers = "0123456789"
        val symbols = ".?,;-_¡!¿*%&$/()[]{}|@><"

        if (blnNumeros) base += numbers
        if (blnSimbolo) base += symbols

        var strResultado: String = generar(base, intDimension)
        return strResultado
    }
}