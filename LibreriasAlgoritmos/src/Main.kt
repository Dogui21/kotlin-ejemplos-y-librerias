//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
fun main() {
    val contrasena = GenerarContrasena();
    contrasena.intDimension = 15;
    contrasena.blnNumeros = true;
    contrasena.blnSimbolo = true;

    println("Contraseña Generada:\n${contrasena.contrasenaAleatoria()}");
}