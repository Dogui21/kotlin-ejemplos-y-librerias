//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
fun main(args: Array<String>) {
    invertirNumero();
}

fun invertirNumero() {
    var intNum: Short;
    var intAux: Short;
    var intDec: Short;
    var intUni: Short;
    var strLinea: String;

    println("Invertir un número de dos cifras");
    println("");
    println("ingrese número de 2 cifras: ")
    strLinea = readLine()!!;

    intNum = strLinea.toShort();
    intDec = (intNum / 10).toShort();
    intUni = (intNum % 10).toShort();
    intAux = (intUni * 10 + intDec).toShort();

    println("Número invertido es: $intAux");
    println("");
}